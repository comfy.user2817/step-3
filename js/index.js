"use strict";

const DATA = [
	{
		"first name": "Олексій",
		"last name": "Петров",
		photo: "./img/trainers/trainer-m1.jpg",
		specialization: "Басейн",
		category: "майстер",
		experience: "8 років",
		description:
			"Олексій має багаторічний досвід роботи з плавцями. Він займається якісною підготовкою спортсменів на міжнародних змаганнях. Його методика базується на новітніх технологіях тренувань.",
	},
	{
		"first name": "Марина",
		"last name": "Іванова",
		photo: "./img/trainers/trainer-f1.png",
		specialization: "Тренажерний зал",
		category: "спеціаліст",
		experience: "2 роки",
		description:
			"Марина спеціалізується на роботі з ваговими тренажерами. Вона розробила унікальну програму для набору м'язової маси. Її клієнти завжди задоволені результатами.",
	},
	{
		"first name": "Ігор",
		"last name": "Сидоренко",
		photo: "./img/trainers/trainer-m2.jpg",
		specialization: "Дитячий клуб",
		category: "інструктор",
		experience: "1 рік",
		description:
			"Ігор працює з дітьми різного віку. Він створив ігрові методики для розвитку координації та спритності. Його уроки завжди цікаві та корисні для малюків.",
	},
	{
		"first name": "Тетяна",
		"last name": "Мороз",
		photo: "./img/trainers/trainer-f2.jpg",
		specialization: "Бійцівський клуб",
		category: "майстер",
		experience: "10 років",
		description:
			"Тетяна є експертом в бойових мистецтвах. Вона проводить тренування для професіоналів і новачків. Її підхід до навчання допомагає спортсменам досягати високих результатів.",
	},
	{
		"first name": "Сергій",
		"last name": "Коваленко",
		photo: "./img/trainers/trainer-m3.jpg",
		specialization: "Тренажерний зал",
		category: "інструктор",
		experience: "1 рік",
		description:
			"Сергій фокусується на роботі з фітнесом та кардіотренуваннями. Він вдосконалив свої методики протягом багатьох років. Його учні завжди в формі та енергійні.",
	},
	{
		"first name": "Олена",
		"last name": "Лисенко",
		photo: "./img/trainers/trainer-f3.jpg",
		specialization: "Басейн",
		category: "спеціаліст",
		experience: "4 роки",
		description:
			"Олена спеціалізується на синхронному плаванні. Вона тренує команди різного рівня. Її команди завжди займають призові місця на змаганнях.",
	},
	{
		"first name": "Андрій",
		"last name": "Волков",
		photo: "./img/trainers/trainer-m4.jpg",
		specialization: "Бійцівський клуб",
		category: "інструктор",
		experience: "1 рік",
		description:
			"Андрій має досвід у вивченні різних бойових мистецтв. Він викладає техніку та тактику бою. Його учні здобувають перемоги на міжнародних турнірах.",
	},
	{
		"first name": "Наталія",
		"last name": "Романенко",
		photo: "./img/trainers/trainer-f4.jpg",
		specialization: "Дитячий клуб",
		category: "спеціаліст",
		experience: "3 роки",
		description:
			"Наталія розробила унікальну програму для дітей дошкільного віку. Вона допомагає дітям розвивати фізичні та ментальні навички. Її класи завжди веселі та динамічні.",
	},
	{
		"first name": "Віталій",
		"last name": "Козлов",
		photo: "./img/trainers/trainer-m5.jpg",
		specialization: "Тренажерний зал",
		category: "майстер",
		experience: "10 років",
		description:
			"Віталій спеціалізується на функціональному тренуванні. Він розробив ряд ефективних тренувальних програм. Його клієнти швидко досягають бажаних результатів.",
	},
	{
		"first name": "Юлія",
		"last name": "Кравченко",
		photo: "./img/trainers/trainer-f5.jpg",
		specialization: "Басейн",
		category: "спеціаліст",
		experience: "4 роки",
		description:
			"Юлія є експертом у водних видах спорту. Вона проводить тренування з аквагімнастики та аеробіки. Її учні демонструють вражаючі показники на змаганнях.",
	},
	{
		"first name": "Олег",
		"last name": "Мельник",
		photo: "./img/trainers/trainer-m6.jpg",
		specialization: "Бійцівський клуб",
		category: "майстер",
		experience: "12 років",
		description:
			"Олег є визнаним майстром в бойових мистецтвах. Він тренує чемпіонів різних вагових категорій. Його методики вважаються одними з найефективніших у світі бойових мистецтв.",
	},
	{
		"first name": "Лідія",
		"last name": "Попова",
		photo: "./img/trainers/trainer-f6.jpg",
		specialization: "Дитячий клуб",
		category: "інструктор",
		experience: "1 рік",
		description:
			"Лідія має великий досвід у роботі з дітьми. Вона організовує різноманітні спортивні ігри та заняття. Її класи завжди допомагають дітям розвивати соціальні навички та командний дух.",
	},
	{
		"first name": "Роман",
		"last name": "Семенов",
		photo: "./img/trainers/trainer-m7.jpg",
		specialization: "Тренажерний зал",
		category: "спеціаліст",
		experience: "2 роки",
		description:
			"Роман є експертом у кросфіту та функціональних тренуваннях. Він розробив власні програми для різних вікових груп. Його учні часто отримують нагороди на фітнес-турнірах.",
	},
	{
		"first name": "Анастасія",
		"last name": "Гончарова",
		photo: "./img/trainers/trainer-f7.jpg",
		specialization: "Басейн",
		category: "інструктор",
		experience: "1 рік",
		description:
			"Анастасія фокусується на водних програмах для здоров'я та фітнесу. Вона проводить тренування для осіб з різним рівнем підготовки. Її учні відзначають покращення здоров'я та благополуччя після занять.",
	},
	{
		"first name": "Валентин",
		"last name": "Ткаченко",
		photo: "./img/trainers/trainer-m8.jpg",
		specialization: "Бійцівський клуб",
		category: "спеціаліст",
		experience: "2 роки",
		description:
			"Валентин є експертом в таеквондо та кікбоксингу. Він викладає техніку, тактику та стратегію бою. Його учні часто стають чемпіонами на національних та міжнародних змаганнях.",
	},
	{
		"first name": "Лариса",
		"last name": "Петренко",
		photo: "./img/trainers/trainer-f8.jpg",
		specialization: "Дитячий клуб",
		category: "майстер",
		experience: "7 років",
		description:
			"Лариса розробила комплексну програму для розвитку фізичних та інтелектуальних навичок дітей. Вона проводить заняття в ігровій формі. Її методика допомагає дітям стати активними та розумними.",
	},
	{
		"first name": "Олексій",
		"last name": "Петров",
		photo: "./img/trainers/trainer-m9.jpg",
		specialization: "Басейн",
		category: "майстер",
		experience: "11 років",
		description:
			"Олексій має багаторічний досвід роботи з плавцями. Він займається якісною підготовкою спортсменів на міжнародних змаганнях. Його методика базується на новітніх технологіях тренувань.",
	},
	{
		"first name": "Марина",
		"last name": "Іванова",
		photo: "./img/trainers/trainer-f9.jpg",
		specialization: "Тренажерний зал",
		category: "спеціаліст",
		experience: "2 роки",
		description:
			"Марина спеціалізується на роботі з ваговими тренажерами. Вона розробила унікальну програму для набору м'язової маси. Її клієнти завжди задоволені результатами.",
	},
	{
		"first name": "Ігор",
		"last name": "Сидоренко",
		photo: "./img/trainers/trainer-m10.jpg",
		specialization: "Дитячий клуб",
		category: "інструктор",
		experience: "1 рік",
		description:
			"Ігор працює з дітьми різного віку. Він створив ігрові методики для розвитку координації та спритності. Його уроки завжди цікаві та корисні для малюків.",
	},
	{
		"first name": "Наталія",
		"last name": "Бондаренко",
		photo: "./img/trainers/trainer-f10.jpg",
		specialization: "Бійцівський клуб",
		category: "майстер",
		experience: "8 років",
		description:
			"Наталія є майстром у бойових мистецтвах. Вона вивчала різні техніки та стили, із якими працює зі своїми учнями. Її підхід до навчання відповідає найвищим стандартам.",
	},
	{
		"first name": "Андрій",
		"last name": "Семенов",
		photo: "./img/trainers/trainer-m11.jpg",
		specialization: "Тренажерний зал",
		category: "інструктор",
		experience: "1 рік",
		description:
			"Андрій спеціалізується на функціональному тренуванні. Він розробив власну систему вправ для зміцнення корпусу. Його учні завжди отримують видимі результати.",
	},
	{
		"first name": "Софія",
		"last name": "Мельник",
		photo: "./img/trainers/trainer-f11.jpg",
		specialization: "Басейн",
		category: "спеціаліст",
		experience: "6 років",
		description:
			"Софія працює з аквагімнастикою. Вона вивчила різні техніки та стили плавання. Її заняття допомагають клієнтам розслабитися та покращити фізичну форму.",
	},
	{
		"first name": "Дмитро",
		"last name": "Ковальчук",
		photo: "./img/trainers/trainer-m12.png",
		specialization: "Дитячий клуб",
		category: "майстер",
		experience: "10 років",
		description:
			"Дмитро спеціалізується на розвитку дитячого спорту. Він розробив унікальну програму для малюків. Його методики забезпечують гармонійний розвиток дітей.",
	},
	{
		"first name": "Олена",
		"last name": "Ткаченко",
		photo: "./img/trainers/trainer-f12.jpg",
		specialization: "Бійцівський клуб",
		category: "спеціаліст",
		experience: "5 років",
		description:
			"Олена є відомим тренером у жіночому бойовому клубі. Вона вивчила різні техніки самооборони. Її підхід дозволяє її ученицям відчувати себе впевнено в будь-яких ситуаціях.",
	},
];

//====================================================КАРТКИ 

//контейцнер карток
let cards = document.querySelector('.trainers-cards__container');

//массив данних що відображаються
let trueArray = []

//Створення та заповнення карток
function CardsAdd(Array) {
	cards.innerHTML = ''
	let template = document.getElementById('trainer-card');

	Array.forEach((element, index) => {
		////створення карток
		let card = template.content.cloneNode(true)
		cards.appendChild(card)
		if(Array != trueArray){
			trueArray.push(element)
		}
		////заповнення карток
		const { "first name": firstName, "last name": lastName, photo } = element;
		
		let imgArray = document.querySelectorAll('.trainer__img')
		let nameArray = document.querySelectorAll('.trainer__name')

		imgArray[index].src = photo;
		nameArray[index].textContent = `${firstName} ${lastName}`;
		
	});
}

//Функція для заповнення массиву
function Pusher(phatherArray,dotherArray){
	dotherArray.length = 0
	phatherArray.forEach((element)=>{
		dotherArray.push(element)
	})
}



//===================================================SORTING

//поле сортування
const sorting = document.querySelector('.sorting')
sorting.removeAttribute('hidden')

//кнопки сортування і їх массив
const [p, sortNull, sortName, sortExp] = sorting.children
const sortBtns = [sortNull,sortName,sortExp]

//массив сортувань
let sortingArray =[]

//Функція сортування за імям
function NameSorting(Array) {
	Array.sort((a, b) => {
		const lastNameA = a["last name"].toLowerCase();
		const lastNameB = b["last name"].toLowerCase();
		return lastNameA.localeCompare(lastNameB);
	});
	Pusher(Array,sortingArray)
}

//Функція сортування за досвідом
function ExpSorting(Array) {
	Array.sort((a, b) => 
		parseInt(b.experience) - parseInt(a.experience));
	Pusher(Array,sortingArray)
}

function Sorting(){
	
	//івент сортування
	sorting.addEventListener('click', (event)=>{
		sortingArray.length = 0
		// trueArray.length = 0

		//умова щоб івент активовував тільки кнопки сортувань
		if(event.target.classList.contains('sorting__btn')){
			////перебор кнопок сортувань
			sortBtns.forEach((element)=>{
				////деактивація активної кнопки
				element.classList.remove('sorting__btn--active');
				////активація івентної кнопки
				event.target.classList.add('sorting__btn--active');
			})
		}
		
		//=============Умови сортувань (заповнення sortingArray)
		
		//якщо сортування і фільтрація вимкнені
		if(sortNull.classList.contains('sorting__btn--active')&&(specActive == 'ВСІ' && catActive == 'ВСІ')) {
			Pusher(DATA,trueArray)
			Pusher(trueArray,sortingArray)
		}
		//якщо сортування вимкнено
		if(sortNull.classList.contains('sorting__btn--active')) {
			//і якщо фільтрація вимкнена
			if(specActive == 'ВСІ' && catActive == 'ВСІ') {
				Pusher(filterArray,trueArray)
				Pusher(filterArray,sortingArray)
			}
			//і якщо сортування вимкнено, а фільтрація увімкнена
			else{ 
				FilterCondit(DATA)
				Pusher(filterArray, trueArray)
				Pusher(trueArray,sortingArray)
			}
		}

		//якщо сортування за іменем
		if(sortName.classList.contains('sorting__btn--active')) {
			//і якщо фільтрація вимкнена
			if(specActive == 'ВСІ' && catActive == 'ВСІ') {
				Pusher(DATA,trueArray)
				NameSorting(trueArray)
			}
			else{
				NameSorting(filterArray)
				Pusher(sortingArray,trueArray)
			}
		}
		//якщо сортування за досвідом
		if(sortExp.classList.contains('sorting__btn--active')) {
			//і якщо фільтрація вимкнена
			if(specActive == 'ВСІ' && catActive == 'ВСІ') {
				Pusher(DATA,trueArray)
				ExpSorting(trueArray)	
			}
			else{
				ExpSorting(filterArray)
				Pusher(sortingArray,trueArray)
			}
		}

		//заповнення карт згідно сортованого массиву
		CardsAdd(trueArray)
	
		//відкриття модального вікна
		ModalWindow(trueArray)
		
	})
}



//===================================================FILTERS
const sidebar = document.querySelector('.sidebar')
sidebar.removeAttribute('hidden')

//фільтри
const filtersForm = document.querySelector('.filters')
const [Spec,Cat,filtersBtn] = filtersForm.children
const [...specArray] = Spec.children
const filtersSpec = [...specArray]
const [...catArray] = Cat.children
const filtersCat = [...catArray]

//массив фільтрів
let filterArray = []

//активні фільтри
let specActive = 'ВСІ';
let catActive = 'ВСІ';

//Функція в умови фыльтрів
function FilterCondit(Array) {
	filterArray.length = 0
	Array.forEach((element)=>{
		//текстовий контент елементів перебору для порівняння з активними фільтрами
		let elementSpec = element.specialization.toUpperCase().replace(/\s+/g, '')
		let elementCat = element.category.toUpperCase()

		//якщо фільтр спеціалізаціі неактивний
		if(specActive == 'ВСІ' && catActive != 'ВСІ'){
			if(elementCat == catActive){
				filterArray.push(element)
			}
		}
		//якщо фільтр категоріі неактивний
		if(catActive == 'ВСІ'&& specActive != 'ВСІ'){
			if(elementSpec == specActive){
				filterArray.push(element)
			}
		}
		//якщо всі фільтри активні
		if((elementSpec == specActive)&&(elementCat == catActive)){
			filterArray.push(element)
		}
	
	})
}

function Filter(){

	//Івент фільтрів
	filtersForm.addEventListener('submit', (event)=>{
		event.preventDefault();
		// filterArray.length = 0
		
		
		//активний фільтр НАПРЯМУ
		filtersSpec.forEach((element)=>{
			if(element.checked){
				specActive = element.nextElementSibling.textContent.replace(/\s+/g, '')
			}
		})
		//активний фільтр КАТЕГОРІІ
		filtersCat.forEach((element)=>{
			if(element.checked){
				catActive = element.nextElementSibling.textContent.replace(/\s+/g, '')
			}
		})


		//================================Умови фільтрів	
	

		//якщо сортування вимкнено
		if(sortNull.classList.contains('sorting__btn--active')||(sortingArray.length = 0)) {
			FilterCondit(DATA)
			Pusher(filterArray, trueArray)
		}
		else{
			if(sortingArray.length != 0) {
				FilterCondit(sortingArray)
				Pusher(filterArray, trueArray)
			}
			else{
				FilterCondit(DATA)
				Pusher(filterArray, trueArray)
			}
		}
		
		//Якщо обидва фільтри неактивні
		if(specActive == 'ВСІ' && catActive == 'ВСІ') {
			//і якщо сортування вимкнено
			if(sortNull.classList.contains('sorting__btn--active')) {
				Pusher(DATA, trueArray)
			}
			else {
				Pusher(sortingArray, trueArray)
			}
		}

		//заповнення карт згідно сортованого массиву
		CardsAdd(trueArray)

		//відкриття модального вікна
		ModalWindow(trueArray)
	})
}


//=====================================================MODAL
function ModalWindow(){
	
	//===========Кастилі для модільного вікна (додаю дів щоб потім очистити його)
	const modalCrutch = document.createElement('div')
	document.body.appendChild(modalCrutch)
	
	//=======================================

	//массив кнопок
	let cardsButtons = document.querySelectorAll('.trainer__show-more')
	
	//перебор кнопок і івент на відкриття модального вікна
	cardsButtons.forEach((button, index) =>{
		button.addEventListener('click', ()=> {
			document.body.style.overflow = 'hidden'
			modalCrutch.innerHTML = ''
			//клонування і додавання модального вікна 
			let modal = document.getElementById('modal-template')
			const modalWin = modal.content.cloneNode(true)
			modalCrutch.appendChild(modalWin)
	
			//елементи модального вікна
			let modBody = document.querySelector('.modal__body')
			const [modImg, modDiscrept] = modBody.children
			const [modName, modCat, modExp, modSpec, modText] = modDiscrept.children

			//елементи массиву карток
			const { "first name": firstName, "last name": lastName, photo, specialization,category, experience, description} = trueArray[index];
				
			//заповнення модального вікна
			modImg.src = photo
			modName.textContent = `${firstName} ${lastName}`;
			modCat.textContent = `Категорія: ${category}`;
			modExp.textContent = `Досвід: ${experience}`
			modSpec.textContent = `Напрям тренера: ${specialization}`
			modText.textContent = description
			
			//конопка і івент закриття модального вікна
			const closeBut = document.querySelector('.modal__close')
			closeBut.addEventListener('click',()=>{
				document.body.style.overflow = "auto"
				modalCrutch.innerHTML = ''
			})
		})
	})
}


CardsAdd(DATA)

Sorting()
Filter()

ModalWindow()